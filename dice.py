#!/usr/bin/python3
# requires qiskit
# thanks to https://towardsdatascience.com/flip-a-coin-on-a-real-quantum-computer-in-python-df51e5f2367b

from qiskit import *

# Create circuit with 1 quantum and 1 classical bit
circuit = QuantumCircuit(1, 1)
# Apply Hadamard gate to quantum bit --> Superposition
circuit.h(0)
# Measure quantum bit and store result in classical bit
circuit.measure(0, 0)

provider = IBMQ.enable_account("86c5b7d3a211c2ba428c03f11c8478e0f618f07c2b1c5da02826aafac99cbdefaefd3ae4522cf390d95324684c1e66b703d9a7bf88fc531f2c1dd72ff202a2e1")
quantum_computer = provider.get_backend("ibmq_armonk")
job = execute(circuit, quantum_computer, shots=1)

# Extract result and print it
print(job.result())
counts = job.result().get_counts()


