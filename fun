#!/bin/bash
VOICE="-v en+f3"
while true; do
    espeak -a 30 $VOICE "half an hour of fun"
    termdown 30m -c 3 -T "fun" -e
    espeak -a 30 $VOICE "half an hour of celebration"
    termdown 30m -c 3 -T "celebration" -e
done
