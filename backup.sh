rsync -ah root@thecla:/ --exclude=/proc --exclude=/sys --exclude=/dev ${HOME}/backups/thecla/
rsync -ah root@dorcas:/ --exclude=/proc --exclude=/sys --exclude=/dev --exclude=/var ${HOME}/backups/dorcas/
